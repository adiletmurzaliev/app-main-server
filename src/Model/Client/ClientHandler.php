<?php

namespace App\Model\Client;


use App\Entity\Client;
use App\Entity\Landlord;
use App\Entity\Tenant;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createClient(array $data): Client
    {
        $client = new Client();
        $this->createAbstractClient($client, $data);

        return $client;
    }

    public function createTenant(array $data): Tenant
    {
        /** @var Tenant $client */
        $client = $this->createAbstractClient(new Tenant(), $data);

        return $client;
    }

    public function createLandlord(array $data): Landlord
    {
        /** @var Landlord $client */
        $client = $this->createAbstractClient(new Landlord(), $data);

        return $client;
    }

    public function createAbstractClient(Client $client, array $data)
    {
        $client
            ->setEmail($data['email'])
            ->setPassport($data['passport'])
            ->setPassword($data['password'])
            ->setRegisteredAt($data['registeredAt'])
            ->setVkId($data['vkId'] ?? null)
            ->setFaceBookId($data['faceBookId'] ?? null)
            ->setGoogleId($data['googleId'] ?? null)
            ->setRoles($data['roles'] ?? ['ROLE_USER']);

        return $client;
    }

    public function encodePassword(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function isPasswordValid(string $plainPassword, string $encodedPassword): bool
    {
        return password_verify($plainPassword, $encodedPassword);
    }
}