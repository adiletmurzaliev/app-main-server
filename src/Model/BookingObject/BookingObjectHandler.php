<?php
/**
 * Created by PhpStorm.
 * User: adilet
 * Date: 05.07.18
 * Time: 15:41
 */

namespace App\Model\BookingObject;


use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BookingObjectHandler
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createPension(array $data)
    {
        /** @var Pension $pension */
        $pension = $this->createAbstractBookingObject(new Pension(), $data);
        $pension
            ->setPool($data['pool'])
            ->setCinema($data['cinema'])
            ->setDescription($data['description']);

        return $pension;
    }

    public function createCottage(array $data)
    {
        /** @var Cottage $cottage */
        $cottage = $this->createAbstractBookingObject(new Cottage(), $data);
        $cottage
            ->setKitchen($data['kitchen'])
            ->setShower($data['shower']);

        return $cottage;
    }

    public function createAbstractBookingObject(BookingObject $bookingObject, array $data)
    {
        $bookingObject
            ->setTitle($data['title'])
            ->setRoomsNumber($data['roomsNumber'])
            ->setManager($data['manager'])
            ->setPhone($data['phone'])
            ->setAddress($data['address'])
            ->setPrice($data['price']);

        return $bookingObject;
    }

}