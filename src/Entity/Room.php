<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 */
class Room
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $roomNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $rentDate;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $clientPassport;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $clientEmail;

    /**
     * @var BookingObject
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\BookingObject", inversedBy="rooms")
     */
    private $bookingObject;


    public function __construct()
    {
        $this->rentDate = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setRoomNumber(?string $roomNumber): Room
    {
        $this->roomNumber = $roomNumber;
        return $this;
    }

    public function getRoomNumber(): ?string
    {
        return $this->roomNumber;
    }

    public function setRentDate(\DateTime $rentDate): Room
    {
        $this->rentDate = $rentDate;
        return $this;
    }

    public function getRentDate(): \DateTime
    {
        return $this->rentDate;
    }

    public function setClientPassport(?string $clientPassport): Room
    {
        $this->clientPassport = $clientPassport;
        return $this;
    }

    public function getClientPassport(): ?string
    {
        return $this->clientPassport;
    }

    public function setClientEmail(?string $clientEmail): Room
    {
        $this->clientEmail = $clientEmail;
        return $this;
    }

    public function getClientEmail(): ?string
    {
        return $this->clientEmail;
    }

    public function setBookingObject(BookingObject $bookingObject): Room
    {
        $this->bookingObject = $bookingObject;
        return $this;
    }

    public function getBookingObject(): BookingObject
    {
        return $this->bookingObject;
    }

    public function __toArray()
    {
        return [
            'id' => $this->getId(),
            'roomNumber' => $this->getRoomNumber(),
            'clientPassport' => $this->getClientPassport(),
            'clientEmail' => $this->getClientEmail(),
            'rentDate' => $this->getRentDate()
        ];
    }
}
