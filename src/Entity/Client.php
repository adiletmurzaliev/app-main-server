<?php

namespace App\Entity;

use App\Model\Client\ClientHandler;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"Client" = "Client", "Tenant" = "Tenant", "Landlord" = "Landlord"})
 * @UniqueEntity("email", message="Такой email уже используется")
 * @UniqueEntity("passport", message="Пользователь с такими данными уже зарегестрирован")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $roles;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=254, unique=true)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Email(message="Введен неправильный email")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, unique=true)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $passport;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vkId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $faceBookId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now");
        $this->roles = json_encode(['ROLE_USER']);
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     * @return Client
     */
    public function setRegisteredAt(\DateTime $registeredAt): Client
    {
        $this->registeredAt = $registeredAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }

    public function addRole(string $role)
    {
        $roles = json_decode($this->roles, true);
        $roles[] = $role;
        $this->roles = json_encode($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getUsername(): ?string
    {
        return $this->getEmail();
    }

    public function setUsername(?string $username): Client
    {
        return $this->setEmail($username);
    }

    public function setPassword(?string $password): Client
    {
        $this->password = $password;
        return $this;
    }

    public function setEmail(?string $email): Client
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setPlainPassword(?string $plainPassword): Client
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPassport(?string $passport): Client
    {
        $this->passport = $passport;
        return $this;
    }

    public function getPassport(): ?string
    {
        return $this->passport;
    }

    public function setVkId(?string $vkId): Client
    {
        $this->vkId = $vkId;
        return $this;
    }

    public function getVkId(): ?string
    {
        return $this->vkId;
    }

    public function setFaceBookId(?string $faceBookId): Client
    {
        $this->faceBookId = $faceBookId;
        return $this;
    }

    public function getFaceBookId(): ?string
    {
        return $this->faceBookId;
    }

    public function setGoogleId(?string $googleId): Client
    {
        $this->googleId = $googleId;
        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setSocialId($network, $id) {
        switch($network) {
            case ClientHandler::SOC_NETWORK_VKONTAKTE:
                $this->vkId = $id;
                break;
            case ClientHandler::SOC_NETWORK_FACEBOOK:
                $this->faceBookId = $id;
                break;
            case ClientHandler::SOC_NETWORK_GOOGLE:
                $this->googleId = $id;
                break;
        }
        return $this;
    }

    public function getSocialId(string $network) {
        switch($network) {
            case ClientHandler::SOC_NETWORK_VKONTAKTE:
                return $this->vkId;
                break;
            case ClientHandler::SOC_NETWORK_FACEBOOK:
                return $this->faceBookId;
                break;
            case ClientHandler::SOC_NETWORK_GOOGLE:
                return $this->googleId;
                break;
            default:
                return null;
        }
    }

    public function __toArray() {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'passport' => $this->passport,
            'vkId' => $this->vkId,
            'faceBookId' => $this->faceBookId,
            'googleId' => $this->googleId,
            'registeredAt' => $this->registeredAt
        ];
    }
}
