<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends BookingObject
{
    const TYPE = 'Пансионат';

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @Assert\NotNull(message="Поле не может быть пустым")
     */
    private $pool;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @Assert\NotNull(message="Поле не может быть пустым")
     */
    private $cinema;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $description;


    public function setPool(?bool $pool): Pension
    {
        $this->pool = $pool;
        return $this;
    }

    public function isPool(): ?bool
    {
        return $this->pool;
    }

    public function setCinema(?bool $cinema): Pension
    {
        $this->cinema = $cinema;
        return $this;
    }

    public function isCinema(): ?bool
    {
        return $this->cinema;
    }

    public function setDescription(?string $description): Pension
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function __toArray() {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'roomsNumber' => $this->getRoomsNumber(),
            'manager' => $this->getManager(),
            'phone' => $this->getPhone(),
            'address' => $this->getAddress(),
            'price' => $this->getPrice(),
            'pool' => $this->isPool(),
            'cinema' => $this->isCinema(),
            'description' => $this->getDescription(),
            'type' => self::TYPE
        ];
    }
}
