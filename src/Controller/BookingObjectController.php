<?php

namespace App\Controller;

use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\Room;
use App\Model\BookingObject\BookingObjectHandler;
use App\Repository\BookingObjectRepository;
use App\Repository\CottageRepository;
use App\Repository\PensionRepository;
use App\Repository\RoomRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/booking-object")
 *
 * Class BookingObjectController
 * @package App\Controller
 */
class BookingObjectController extends Controller
{
    /**
     * @Route("/exists/{title}", name="booking_object_exists")
     * @Method("HEAD")
     *
     * @param string $title
     * @param BookingObjectRepository $bookingObjectRepository
     * @return Response
     */
    public function exists(string $title, BookingObjectRepository $bookingObjectRepository): Response
    {
        $bookingObject = $bookingObjectRepository->findOneByTitle($title);

        if ($bookingObject) {
            return new JsonResponse(true);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/create-pension", name="booking_object_create_pension")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param BookingObjectHandler $bookingObjectHandler
     * @return Response
     */
    public function createPension(
        Request $request,
        EntityManagerInterface $em,
        BookingObjectHandler $bookingObjectHandler
    ): Response
    {
        $data = $request->request->all();
        $pension = $bookingObjectHandler->createPension($data);

        $rooms = [];
        for ($i = 1; $i <= $data['roomsNumber']; $i++) {
            $room = new Room();
            $room
                ->setRoomNumber($i)
                ->setBookingObject($pension);
            $rooms[$i] = $room;
            $em->persist($rooms[$i]);
        }

        $em->persist($pension);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/create-cottage", name="booking_object_create_cottage")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param BookingObjectHandler $bookingObjectHandler
     * @return Response
     */
    public function createCottage(
        Request $request,
        EntityManagerInterface $em,
        BookingObjectHandler $bookingObjectHandler
    ): Response
    {
        $data = $request->request->all();
        $cottage = $bookingObjectHandler->createCottage($data);

        $rooms = [];
        for ($i = 1; $i <= $data['roomsNumber']; $i++) {
            $room = new Room();
            $room
                ->setRoomNumber($i)
                ->setBookingObject($cottage);
            $rooms[$i] = $room;
            $em->persist($rooms[$i]);
        }

        $em->persist($cottage);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/get-all", name="booking_object_get_all")
     * @Method("GET")
     *
     * @param BookingObjectRepository $bookingObjectRepository
     * @return Response
     */
    public function getAllBookingObjects(BookingObjectRepository $bookingObjectRepository): Response
    {
        $bookingObjects = $bookingObjectRepository->findAllLatest();
        $data = $this->objectsToArray($bookingObjects);

        return new JsonResponse($data);
    }

    /**
     * @Route("/get-all-filter", name="booking_object_get_all_filter")
     * @Method("GET")
     *
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @param PensionRepository $pensionRepository
     * @param CottageRepository $cottageRepository
     * @return Response
     */
    public function getAllBookingObjectsByFilter(
        Request $request,
        BookingObjectRepository $bookingObjectRepository,
        PensionRepository $pensionRepository,
        CottageRepository $cottageRepository
    ): Response
    {
        $query = $request->query->all();

        if (isset($query['booking_type'])) {

            switch ($query['booking_type']) {
                case Pension::TYPE:
                    $bookingObjects = $pensionRepository->findAllByFilter($query);
                    break;
                case Cottage::TYPE:
                    $bookingObjects = $cottageRepository->findAllByFilter($query);
                    break;
                default:
                    $bookingObjects = $bookingObjectRepository->findAllByFilter($query);
                    break;
            }
        } else {
            $bookingObjects = $bookingObjectRepository->findAllByFilter($query);
        }

        if ($bookingObjects) {
            $data = $this->objectsToArray($bookingObjects);
            return new JsonResponse($data);
        }

        return new JsonResponse(null);
    }

    /**
     * @Route("/get-data/{title}", name="booking_object_get_data")
     * @Route("GET")
     *
     * @param BookingObjectRepository $bookingObjectRepository
     * @param string $title
     * @return Response
     */
    public function getBookingObjectData(
        BookingObjectRepository $bookingObjectRepository,
        string $title
    ): Response
    {
        $bookingObject = $bookingObjectRepository->findOneByTitle(urldecode($title));

        if ($bookingObject) {
            $rooms = $this->objectsToArray($bookingObject->getRooms()->toArray());
            $data = $bookingObject->__toArray();
            $data['rooms'] = $rooms;
            return new JsonResponse($data);
        }

        return new JsonResponse(null);
    }

    /**
     * @Route("/book", name="booking_object_book")
     * @Method("PATCH")
     *
     * @param Request $request
     * @param RoomRepository $roomRepository
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function book(
        Request $request,
        RoomRepository $roomRepository,
        EntityManagerInterface $manager
    ): Response
    {
        $data = $request->request->all();

        /** @var Room $room */
        $room = $roomRepository->findOneByRoomNumberAndBookingTitle($data['title'], $data['roomNumber']);

        if ($room) {
            $room
                ->setRentDate(new \DateTime($data['rentDate']))
                ->setClientEmail($data['clientEmail'])
                ->setClientPassport($data['clientPassport']);

            $manager->persist($room);
            $manager->flush();

            return new JsonResponse(true);
        }
        return new JsonResponse(false);
    }

    private function objectsToArray(array $objects)
    {
        $result = [];

        foreach ($objects as $object) {
            $result[] = $object->__toArray();
        }

        return $result;
    }
}