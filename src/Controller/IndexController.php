<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_index")
     */
    public function uploadAction()
    {
        return new Response("Доброго дня. Вы видите индексную страницу API онлайн бронирования");
    }

    /**
     * @Route("/ping", name="app_ping")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }
}
