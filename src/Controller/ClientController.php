<?php

namespace App\Controller;

use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/client")
 *
 * Class ClientController
 * @package App\Controller
 */
class ClientController extends Controller
{
    /**
     * @Route("/exists/{passport}/{email}", name="client_exist")
     * @Method("HEAD")
     *
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function exists(string $passport, string $email, ClientRepository $clientRepository): Response
    {
        $client = $clientRepository->findOneByPassportOrEmail($passport, $email);

        if ($client) {
            return new JsonResponse(true);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/create", name="client_create")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param ClientHandler $clientHandler
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $em, ClientHandler $clientHandler): Response
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');
        $data['registeredAt'] = new \DateTime($request->request->get('registeredAt')['date']);
        $data['roles'] = $request->request->get('roles');

        if (in_array('ROLE_TENANT', $data['roles'])) {
            $client = $clientHandler->createTenant($data);
        } elseif (in_array('ROLE_LANDLORD', $data['roles'])) {
            $client = $clientHandler->createLandlord($data);
        } else {
            $client = $clientHandler->createClient($data);
        }

        $em->persist($client);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/get-data/{email}", name="client_get_data")
     * @Method("GET")
     *
     * @param ClientRepository $clientRepository
     * @param string $email
     * @return Response
     */
    public function getData(ClientRepository $clientRepository, string $email): Response
    {
        $client = $clientRepository->findOneByEmail($email);

        if ($client) {
            return new JsonResponse($client->__toArray());
        }

        return new JsonResponse(null);
    }

    /**
     * @Route("/check-credentials/{email}/{plainPassword}", name="client_check_credentials")
     * @Method("HEAD")
     *
     * @param string $email
     * @param string $plainPassword
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @return Response
     */
    public function checkCredentials(
        string $email,
        string $plainPassword,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler
    ): Response
    {
        $client = $clientRepository->findOneByEmail($email);

        if ($client && $clientHandler->isPasswordValid($plainPassword, $client->getPassword())) {
            return new JsonResponse(true);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/password-encode", name="client_password_encode")
     * @Method("GET")
     *
     * @param Request $request
     * @param ClientHandler $clientHandler
     * @return Response
     */
    public function passwordEncode(Request $request, ClientHandler $clientHandler): Response
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }

    /**
     * @Route("/connect-social", name="client_connect_social")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function connectSocial(
        Request $request,
        EntityManagerInterface $manager,
        ClientRepository $clientRepository
    ): Response
    {
        $email = $request->request->get('email');
        $passport = $request->request->get('passport');
        $network = $request->request->get('network');
        $uid = $request->request->get('uid');

        $client = $clientRepository->findOneByPassportOrEmail($passport, $email);
        $client->setSocialId($network, $uid);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/exists-by-social/{network}/{uid}", name="client_exist_by_social")
     * @Method("HEAD")
     *
     * @param string $network
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function existsBySocial(string $network, string $uid, ClientRepository $clientRepository): Response
    {
        $client = $clientRepository->findBySocialId($network, $uid);

        if ($client) {
            return new JsonResponse(true);
        } else {
            throw new NotFoundHttpException();
        }
    }
}
