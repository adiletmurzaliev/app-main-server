<?php

namespace App\DataFixtures;

use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\Room;
use App\Model\BookingObject\BookingObjectHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BookingObjectFixtures extends Fixture
{
    /**
     * @var BookingObjectHandler
     */
    private $bookingObjectHandler;

    public function __construct(BookingObjectHandler $bookingObjectHandler)
    {
        $this->bookingObjectHandler = $bookingObjectHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var Pension $pension */
        $pension = $this->bookingObjectHandler->createPension([
            'title' => 'Ак-Марал',
            'roomsNumber' => 5,
            'manager' => 'Иванов С.В.',
            'phone' => '+996 (770) 33-33-33',
            'address' => 'село Бостери',
            'price' => 4000,
            'pool' => true,
            'cinema' => false,
            'description' => 'Описание Ак-Марала'
        ]);

        $rooms = [];
        for ($i = 1; $i <= 5; $i++) {
            $room = new Room();
            $room
                ->setRoomNumber($i)
                ->setBookingObject($pension);
            $rooms[$i] = $room;
            $manager->persist($rooms[$i]);
        }

        $manager->persist($pension);
        $manager->flush();

        /** @var Pension $pension */
        $pension = $this->bookingObjectHandler->createPension([
            'title' => 'Аврора',
            'roomsNumber' => 6,
            'manager' => 'Ибрагимов А.А.',
            'phone' => '+996 (555) 12-32-44',
            'address' => 'г. Чолпон-Ата',
            'price' => 5000,
            'pool' => true,
            'cinema' => true,
            'description' => 'Описание Авроры'
        ]);

        $rooms = [];
        for ($i = 1; $i <= 6; $i++) {
            $room = new Room();
            $room
                ->setRoomNumber($i)
                ->setBookingObject($pension);
            $rooms[$i] = $room;
            $manager->persist($rooms[$i]);
        }

        $manager->persist($pension);
        $manager->flush();

        /** @var Pension $pension */
        $pension = $this->bookingObjectHandler->createPension([
            'title' => 'Солнышко',
            'roomsNumber' => 10,
            'manager' => 'Солнышев А.А.',
            'phone' => '+996 (505) 11-11-41',
            'address' => 'г. Чолпон-Ата',
            'price' => 7000,
            'pool' => true,
            'cinema' => false,
            'description' => 'Описание Солнышка'
        ]);

        $rooms = [];
        for ($i = 1; $i <= 10; $i++) {
            $room = new Room();
            $room
                ->setRoomNumber($i)
                ->setBookingObject($pension);
            $rooms[$i] = $room;
            $manager->persist($rooms[$i]);
        }

        $manager->persist($pension);
        $manager->flush();

        /** @var Cottage $cottage */
        $cottage = $this->bookingObjectHandler->createCottage([
            'title' => '4 сезона',
            'roomsNumber' => 4,
            'manager' => 'Иванова С.В.',
            'phone' => '+996 (554) 20-33-33',
            'address' => 'село Бостери',
            'price' => 1500,
            'kitchen' => true,
            'shower' => false
        ]);

        $rooms = [];
        for ($i = 1; $i <= 4; $i++) {
            $room = new Room();
            $room
                ->setRoomNumber($i)
                ->setBookingObject($cottage);
            $rooms[$i] = $room;
            $manager->persist($rooms[$i]);
        }

        $manager->persist($cottage);
        $manager->flush();
    }
}