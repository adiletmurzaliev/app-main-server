<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{

    /** @var ClientHandler */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createTenant([
            'email' => 'client1@mail.com',
            'passport' => 'ANclient1',
            'password' => $this->clientHandler->encodePassword('123'),
            'registeredAt' => new \DateTime(),
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client);
        $manager->flush();

        $client = $this->clientHandler->createTenant([
            'email' => 'client2@mail.com',
            'passport' => 'AHclient2',
            'password' => $this->clientHandler->encodePassword('123'),
            'registeredAt' => new \DateTime(),
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ]);

        $manager->persist($client);
        $manager->flush();

        $client = $this->clientHandler->createLandlord([
            'email' => 'client3@mail.com',
            'passport' => 'AHclient3',
            'password' => $this->clientHandler->encodePassword('123'),
            'registeredAt' => new \DateTime(),
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ]);

        $manager->persist($client);
        $manager->flush();
    }
}