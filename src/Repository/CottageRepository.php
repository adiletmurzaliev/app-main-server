<?php

namespace App\Repository;

use App\Entity\Cottage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Cottage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cottage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cottage[]    findAll()
 * @method Cottage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CottageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cottage::class);
    }

    public function findAllByFilter(array $query)
    {
        $qb = $this->createQueryBuilder('b');

        if (isset($query['title'])) {
            $qb
                ->select('b')
                ->where('b.title LIKE :title')
                ->setParameter('title', '%' . $query['title'] . '%');
        }

        if (isset($query['minPrice'])) {
            $qb
                ->andWhere('b.price >= :minPrice')
                ->setParameter('minPrice', $query['minPrice']);
        }

        if (isset($query['maxPrice'])) {
            $qb
                ->andWhere('b.price <= :maxPrice')
                ->setParameter('maxPrice', $query['maxPrice']);
        }

        if (isset($query['cottage_choices'])) {
            if (in_array('kitchen', $query['cottage_choices'])) {
                $qb->andWhere('b.kitchen = true');
            }
            if (in_array('shower', $query['cottage_choices'])) {
                $qb->andWhere('b.shower = true');
            }
        }

        return $qb
            ->orderBy('b.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
