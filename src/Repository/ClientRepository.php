<?php

namespace App\Repository;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param string $passport
     * @param string $email
     * @return Client|null
     */
    public function findOneByPassportOrEmail(string $passport, string $email): ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.passport = :passport')
                ->setParameter('passport', $passport)
                ->orWhere('c.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $email
     * @return Client|null
     */
    public function findOneByEmail(string $email): ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $network
     * @param string $uid
     * @return Client|null
     */
    public function findBySocialId(string $network, string $uid): ?Client
    {
        try {
            $result = $this->createQueryBuilder('u')->select('u');

            switch ($network) {
                case ClientHandler::SOC_NETWORK_VKONTAKTE:
                    $result->where('u.vkId = :uid');
                    break;
                case ClientHandler::SOC_NETWORK_FACEBOOK:
                    $result->where('u.faceBookId = :uid');
                    break;
                case ClientHandler::SOC_NETWORK_GOOGLE:
                    $result->where('u.googleId = :uid');
                    break;
            }

            return $result
                ->setParameter('uid', $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
