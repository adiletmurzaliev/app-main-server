<?php

namespace App\Repository;

use App\Entity\Pension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Pension|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pension|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pension[]    findAll()
 * @method Pension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pension::class);
    }

    public function findAllByFilter(array $query)
    {
        $qb = $this->createQueryBuilder('b');

        if (isset($query['title'])) {
            $qb
                ->select('b')
                ->where('b.title LIKE :title')
                ->setParameter('title', '%' . $query['title'] . '%');
        }

        if (isset($query['minPrice'])) {
            $qb
                ->andWhere('b.price >= :minPrice')
                ->setParameter('minPrice', $query['minPrice']);
        }

        if (isset($query['maxPrice'])) {
            $qb
                ->andWhere('b.price <= :maxPrice')
                ->setParameter('maxPrice', $query['maxPrice']);
        }

        if (isset($query['pension_choices'])) {
            if (in_array('pool', $query['pension_choices'])) {
                $qb->andWhere('b.pool = true');
            }
            if (in_array('cinema', $query['pension_choices'])) {
                $qb->andWhere('b.cinema = true');
            }
        }

        return $qb
            ->orderBy('b.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
