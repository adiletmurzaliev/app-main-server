<?php

namespace App\Repository;

use App\Entity\Landlord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Landlord|null find($id, $lockMode = null, $lockVersion = null)
 * @method Landlord|null findOneBy(array $criteria, array $orderBy = null)
 * @method Landlord[]    findAll()
 * @method Landlord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LandlordRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Landlord::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('l')
            ->where('l.something = :value')->setParameter('value', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
