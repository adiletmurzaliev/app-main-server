<?php

namespace App\Repository;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function findOneByTitle(string $title): ?BookingObject
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b')
                ->where('b.title = :title')
                ->setParameter('title', $title)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findAllLatest(): array
    {
        return $this->createQueryBuilder('b')
            ->select('b')
            ->orderBy('b.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllByFilter(array $query)
    {
        $qb = $this->createQueryBuilder('b');

        if (isset($query['title'])) {
            $qb
                ->select('b')
                ->where('b.title LIKE :title')
                ->setParameter('title', '%' . $query['title'] . '%');
        }

        if (isset($query['minPrice'])) {
            $qb
                ->andWhere('b.price >= :minPrice')
                ->setParameter('minPrice', $query['minPrice']);
        }

        if (isset($query['maxPrice'])) {
            $qb
                ->andWhere('b.price <= :maxPrice')
                ->setParameter('maxPrice', $query['maxPrice']);
        }

//        if (isset($query['booking_type'])) {
//
//            switch ($query['booking_type']) {
//                case Pension::TYPE:
//                    $qb->andWhere('b INSTANCE OF App\Entity\Pension');
//                    if (isset($query['pension_choices'])) {
//                        if (in_array('pool', $query['pension_choices'])) {
//                            $qb->andWhere('b.pool = true');
//                        }
//                        if (in_array('cinema', $query['pension_choices'])) {
//                            $qb->andWhere('b.cinema = true');
//                        }
//                    }
//                    break;
//                case Cottage::TYPE:
//                    $qb->andWhere('b INSTANCE OF App\Entity\Cottage');
//                    if (isset($query['cottage_choices'])) {
//                        if (in_array('kitchen', $query['cottage_choices'])) {
//                            $qb->andWhere('b.kitchen = true');
//                        }
//                        if (in_array('shower', $query['cottage_choices'])) {
//                            $qb->andWhere('b.shower = true');
//                        }
//                    }
//                    break;
//            }
//        }

        return $qb
            ->orderBy('b.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
